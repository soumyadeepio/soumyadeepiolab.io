---
layout: about
title: About Me
author: Soumyadeep Das
image: assets/images/about/soumyadeep
permalink: /about
signature: 
---

I am Soumyadeep Das, an incoming doctoral student at ICRAR/ University of Western Australia, Perth. 

I am interested in Active galactic nuclei (AGN) research via multiwavelength studies. I plan to study the evolutionary path of radio galaxies - focussing on questions such as why all young and compact sources do not evolve into giant radio galaxies, what triggers an AGN restart, and the role of the host environment on AGN life-cycle. Further, I am interested in the unified theory of AGN classification. My primary interests lie in imaging and analysis of radio interferometric data of different classes of AGN at different resolutions, frequencies, and sensitivities. 

In the long run, I hope to make a difference in making a career in science more accessible to less fortunate Indian students. 
